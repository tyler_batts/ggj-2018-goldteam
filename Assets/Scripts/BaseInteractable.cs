﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseInteractable : MonoBehaviour {

    [HideInInspector] public bool canInteract;

	public abstract void Interact();

    void OnTriggerStay2D (Collider2D other) {
        if (other.tag == "Player") {
            canInteract = true;
            PlayerController.current.IsAbleToInteract = true;
            PlayerController.current.CurrentInteractable = this;
        }
    }

    void OnTriggerExit2D (Collider2D other) {
        if (other.tag == "Player") {
            canInteract = true;
            PlayerController.current.IsAbleToInteract = false;
            PlayerController.current.CurrentInteractable = null;
        }
    }
}
