﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotController : MonoBehaviour {

    [SerializeField] float speed = 2f;
    [SerializeField] bool isActivatedAtStart;

    [SerializeField] Animator animator;

    Rigidbody2D rb;
    Vector2 targetDirection;
    bool isActivated;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
        isActivated = isActivatedAtStart;
	}

	void Update () {
        animator.SetBool("IsActive", isActivated);
        Vector2 playerPos = PlayerController.current.gameObject.transform.position;
        Vector2 targetPos = new Vector2(playerPos.x, transform.position.y);
        targetDirection = (targetPos - (Vector2)transform.position).normalized;
	}

    void FixedUpdate () {
        if (isActivated) {
            rb.velocity = targetDirection * speed;
        }
    }

    void OnCollisionEnter2D (Collision2D other) {
        if (other.collider.tag == "Player") {
            if (isActivated) {
                LevelController.current.LoseGame();
            } else {
                isActivated = true;
            }
        };
    }
}
