﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinGameMenu : MonoBehaviour {

    public static WinGameMenu current;

	void Awake () {
        current = this;
        this.gameObject.SetActive(false);
    }

	public void RestartLevel () {
        SceneManager.LoadScene("Level1");
    }

    public void QuitToMainMenu () {
        SceneManager.LoadScene("Menu");
    }
}
