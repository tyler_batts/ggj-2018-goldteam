﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {

    public static LevelController current;

    [SerializeField] AudioSource baseAmbient, futureAmbient;

    [SerializeField] int currentAreaIndex = 0;
    [SerializeField] LevelArea[] areas;

    Coroutine futureAmbientMusic;

    GameObject player;

    void Awake () {
        current = this;
    }

    void Start () {
        player = PlayerController.current.gameObject;
        SwitchAreaIndex(currentAreaIndex);

        baseAmbient.Play();
    }

	public void SwitchAreaIndex (int areaIndex) {
        if (areaIndex >= areas.Length) {
            WinGameMenu.current.gameObject.SetActive(true);
            return;
        }

        foreach (LevelArea area in areas) {
            area.BaseArea.SetActive(false);
            area.FutureArea.SetActive(false);
        }

        currentAreaIndex = areaIndex;

        LevelArea newArea = areas[currentAreaIndex];
        newArea.BaseArea.SetActive(true);
        newArea.FutureArea.SetActive(false);
        player.transform.position = areas[currentAreaIndex].PlayerSpawnPoint.transform.position;
    }

    public void SwitchDimensions () {
        LevelArea area = areas[currentAreaIndex];
        area.BaseArea.SetActive(!area.BaseArea.activeInHierarchy);
        area.FutureArea.SetActive(!area.FutureArea.activeInHierarchy);

        if (area.BaseArea.activeInHierarchy) {
            baseAmbient.Play();
            StopCoroutine(futureAmbientMusic);
        } else {
            baseAmbient.Stop();
            futureAmbientMusic = StartCoroutine(RunFutureAmbientMusic());
        }
    }

    public void WinGame () {
        Time.timeScale = 0f;
        WinGameMenu.current.gameObject.SetActive(true);
    }

    public void LoseGame () {
        Time.timeScale = 0f;
        GameOverMenu.current.gameObject.SetActive(true);
    }

    IEnumerator RunFutureAmbientMusic () {
        yield return new WaitForSeconds(Random.Range(1.8f, 4f));
        futureAmbient.pitch = Random.Range(0.95f, 1.1f);
        futureAmbient.Play();
        futureAmbientMusic = StartCoroutine(RunFutureAmbientMusic());
    }

    [System.Serializable]
    private class LevelArea {
        [SerializeField] GameObject baseArea;
        [SerializeField] GameObject futureArea;
        [SerializeField] GameObject playerSpawnPoint;
        [SerializeField] bool isLastAreaInLevel = false;

        public GameObject BaseArea {
            get {
                return baseArea;
            }
        }

        public GameObject FutureArea {
            get {
                return futureArea;
            }
        }

        public GameObject PlayerSpawnPoint {
            get {
                return playerSpawnPoint;
            }
        }
    }
}
