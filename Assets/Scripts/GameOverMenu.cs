﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour {

    public static GameOverMenu current;

    void Awake () {
        current = this;
        this.gameObject.SetActive(false);
    }

	public void RestartFromCheckpoint () {

    }

    public void RestartLevel () {
        SceneManager.LoadScene("Level1");
    }

    public void QuitToMainMenu () {
        SceneManager.LoadScene("Menu");
    }
}
