﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorDoorController : BaseInteractable {

    [SerializeField] AudioSource source;
    [SerializeField] SpriteRenderer renderer;
    [SerializeField] BoxCollider2D collider;

    [SerializeField] bool isOperational = true;

	override public void Interact() {
        if (isOperational) {
            renderer.enabled = !renderer.enabled;
            collider.enabled = !collider.enabled;
            source.Play();
        }
    }
}
