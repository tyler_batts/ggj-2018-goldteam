﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorCarController : BaseInteractable {

    [SerializeField] Animator animator;
    [SerializeField] bool isOperational = true;
    [SerializeField] float animationLength = 2f;

    [SerializeField] AudioSource button, ambient;

    bool isAtBottom = true;
    bool isMoving = false;

    void Update () {}

	override public void Interact() {
        if (isOperational && !isMoving) {
            StartCoroutine(RunElevator());
            button.Play();
        }
    }

    IEnumerator RunElevator () {
        animator.SetBool("IsMoving", !animator.GetBool("IsMoving"));

        animator.SetBool("IsAtBottom", isAtBottom);
        isMoving = true;
        ambient.Play();

        yield return new WaitForSeconds(animationLength);

        isAtBottom = !isAtBottom;
        animator.SetBool("IsMoving", !animator.GetBool("IsMoving"));
        isMoving = false;
        ambient.Stop();
    }
}
