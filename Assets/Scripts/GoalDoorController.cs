﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalDoorController : BaseInteractable {

    [SerializeField] AudioSource source;
    [SerializeField] int areaIndex;

    override public void Interact() {
        source.Play();
        PlayerController.current.PlayGoalWalk();
        Vector2 playerPos = PlayerController.current.gameObject.transform.position;
        PlayerController.current.gameObject.transform.position = new Vector2(transform.position.x, playerPos.y);
        PlayerController.current.rb.velocity = Vector2.zero;
        StartCoroutine(WaitForSoundEffect());
    }

    IEnumerator WaitForSoundEffect () {
        float clipLength = source.clip.length;
        yield return new WaitForSeconds(clipLength + 0.5f);
        LevelController.current.SwitchAreaIndex(areaIndex + 1);
        PlayerController.current.PlayGoalWalkFinish();
    }
}
