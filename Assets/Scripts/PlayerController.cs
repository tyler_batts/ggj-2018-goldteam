﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour {

    public static PlayerController current;

    [SerializeField] AudioClip[] clips;
    [SerializeField] AudioSource walking, jump;
    [SerializeField] float horizontalSpeed = 1f;
    [SerializeField] float maxVerticalSpeed = 7f;
    [SerializeField] float jumpHeight = 7f;
    [SerializeField] LayerMask raycastLayerMask;
    [SerializeField] float raycastDistance = 0.1f;

    Animator animator;
    [HideInInspector] public Rigidbody2D rb;
    bool isGrounded, isAbleToInteract;
    bool canMove = true;
    BaseInteractable currentInteractable;
    RaycastHit2D hit;
    Dictionary<string, AudioClip> audioClips;

	void Awake () {
        current = this;
        Time.timeScale = 1f;
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        audioClips = new Dictionary<string, AudioClip>();

        audioClips.Add("Wood", clips[0]); // Wood walking
        audioClips.Add("Rubble", clips[1]); // Rubble walking
        audioClips.Add("Metal", clips[2]); // Metal walking
	}

	void Update () {
        if (canMove) {
            float horizontal = Input.GetAxis("Horizontal");
            hit = Physics2D.Raycast(transform.position, Vector2.down, raycastDistance, raycastLayerMask);
            Debug.DrawLine(transform.position, (transform.position - (Vector3.up * 0.1f)), Color.red, 0.01f);
            isGrounded = (hit.collider != null && !hit.collider.isTrigger);

            GetComponentInChildren<SpriteRenderer>().flipX = horizontal < 0;
            animator.SetInteger("HorizontalVelocity", Mathf.Abs((int)horizontal));
            animator.SetInteger("VerticalVelocity", (int)rb.velocity.y);
            animator.SetBool("IsGrounded", isGrounded);

            AudioClip clip = null;
            if (hit.collider != null) {
                audioClips.TryGetValue(hit.collider.tag, out clip);
            }

            if (clip != null) {
                walking.clip = clip;
            }

            if (isAbleToInteract && (currentInteractable != null) && Input.GetButtonDown("Interact")) {
                currentInteractable.Interact();
            }

            if (isGrounded && Mathf.Abs(rb.velocity.x) > 0 && !walking.isPlaying) {
                walking.Play();
            }
        }
	}

    void FixedUpdate() {
        if (canMove) {
            float horizontal = Input.GetAxis("Horizontal");
            Vector2 adjustedVelocity;

            if (!isGrounded) {
                adjustedVelocity = new Vector2(horizontal * horizontalSpeed, Mathf.Clamp(rb.velocity.y, -maxVerticalSpeed, maxVerticalSpeed));
            } else {
                Vector2 dirtyVelocity = new Vector2(horizontal * horizontalSpeed, rb.velocity.y);
                adjustedVelocity = Vector3.ProjectOnPlane(dirtyVelocity, hit.normal);
            }

            rb.velocity = adjustedVelocity;

            if (isGrounded && Input.GetButtonDown("Jump")) {
                rb.AddForce(Vector2.up * jumpHeight, ForceMode2D.Impulse);
                jump.Play();
            }
        }
    }

    public void PlayGoalWalk () {
        animator.SetBool("IsGoingThroughGoal", true);
        animator.SetTrigger("GoalEntry");
        canMove = false;
    }

    public void PlayGoalWalkFinish () {
        animator.SetBool("IsGoingThroughGoal", false);
        canMove = true;
    }

    public bool CanMove {
        get {
            return canMove;
        }
    }

    public bool IsAbleToInteract {
        get {
            return isAbleToInteract;
        }

        set {
            isAbleToInteract = value;
        }
    }

    public BaseInteractable CurrentInteractable {
        get {
            return currentInteractable;
        }

        set {
            currentInteractable = value;
        }
    }
}
