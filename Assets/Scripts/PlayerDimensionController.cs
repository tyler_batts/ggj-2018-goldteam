﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDimensionController : MonoBehaviour {

    public static PlayerDimensionController current;

    [SerializeField] AudioSource shift;
    [SerializeField] ParticleSystem particles;

    void Awake () {
        current = this;
    }

	void Update () {
        if (Input.GetButtonDown("DimensionShift") && PlayerController.current.CanMove) {
            LevelController.current.SwitchDimensions();
            shift.Play();
            particles.Play();
        }
	}
}
